package com.bookingflights.tests;

import com.bookingflights.dao.BookingDao;
import com.bookingflights.dao.CollectionBookingDao;
import com.bookingflights.entities.booking.Booking;
import com.bookingflights.entities.flight.Flight;
import com.bookingflights.entities.flight.PlaceDestination;
import com.bookingflights.entities.user.User;
import com.bookingflights.exceptions.IdNotFound;
import com.bookingflights.service.BookingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BookingControllerTest {

  BookingService bookingServiceMock;

  BookingDao bookingDao = new CollectionBookingDao();
  BookingService bookingService = new BookingService(bookingDao);
  List<Booking> testBookingList = new ArrayList<>();
  List<User> testUser = new ArrayList<>();
  Flight testFlight = new Flight(01, "2021-12-21", "12:01", 12, PlaceDestination.NEW_YORK);
  int id = 1111;
  int seat = 12;
  Booking testBooking = new Booking(id, testUser, testFlight, seat);
  User user = new User("Petya", "Ivanov");

  @BeforeEach
  public void setup() {
    bookingService.createNewBooking(id, testUser, testFlight, seat);
  }

  @Test
  public void shouldWeGetAllBookingsTest() {
    assertEquals(testUser, bookingService.getAllBookings().get(0).getUsers());
  }

  @Test
  public void CreateNewBookingTest() {
    assertEquals(false, bookingService.getAllBookings().isEmpty());
  }

  @Test
  public void cancelBookingTest() throws IdNotFound {
    bookingService.cancelBooking(bookingService.getAllBookings().get(0).getId());
    assertEquals(true, bookingService.getAllBookings().isEmpty());
  }

  @Test
  public void getBookingIdTest() {
    assertEquals(false, bookingService.getBookingId(bookingService.getAllBookings().get(0).getId()).isEmpty());
  }

  @Test
  public void getUserTest() {
    assertEquals(testUser, bookingService.getAllBookings().get(0).getUsers());
  }


  @Test
  public void loadDataTest() throws IOException {
    assertEquals(false, bookingService.loadData().isEmpty());
  }
}