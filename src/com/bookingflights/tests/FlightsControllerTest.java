package com.bookingflights.tests;

import com.bookingflights.dao.CollectionFlightDao;
import com.bookingflights.dao.FlightDao;
import com.bookingflights.entities.flight.Flight;
import com.bookingflights.entities.flight.PlaceDestination;
import com.bookingflights.exceptions.IdNotFound;
import com.bookingflights.service.FlightsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FlightsControllerTest {

  FlightsService flightServiceMock;

  FlightDao flightDao = new CollectionFlightDao();
  FlightsService flightsService = new FlightsService(flightDao);

  int seat = 12;
  Flight testFlight = new Flight(1, "2021-12-21", "12:01", seat, PlaceDestination.NEW_YORK);


  @BeforeEach
  public void setup() {
    flightsService.createNewFlight(testFlight);
  }

  @Test
  void getAllFlights() {
    System.out.println(flightsService.getAllFlights().get(0));
    assertEquals(testFlight, flightsService.getAllFlights().get(0));
  }

//    @Test
//    void displayAllFlights() {
//        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
//        System.setOut(new PrintStream(outContent));
//        flightsService.displayAllFlights();
//        String expectedOutput = "Flight{ place destination:New York from Kiev, dispatch date : 2021-12-21 12:01 free places:12 id " +
//                "flight: 1}";
//        assertTrue(outContent.toString().contains(expectedOutput));
//    }

  @Test
  void createNewFlight() {
    assertEquals(false, flightsService.getAllFlights().isEmpty());
  }

  @Test
  void searchFlight() {
    assertEquals(testFlight, flightsService.getAllFlights().get(0));
  }


  @Test
  void getFlightById() {
    assertEquals(testFlight, flightsService.getFlightById(flightsService.getAllFlights().get(0).getId()).get());
  }

  @Test
  void loadData() throws IOException {
//        assertEquals(false, flightsService.loadData().isEmpty());
  }
}