package com.bookingflights.controller;

import com.bookingflights.entities.flight.Flight;
import com.bookingflights.entities.flight.PlaceDestination;
import com.bookingflights.service.FlightsService;

import java.util.List;
import java.util.Optional;

public class FlightsController {
  private FlightsService flightsService;

  public FlightsController(FlightsService flightsService) {
    this.flightsService = flightsService;
  }

  public List<Flight> getAllFlights() {
    return flightsService.getAllFlights();
  }

  public void displayAllFlights() {
    flightsService.displayAllFlights();
  }

  public void createNewFlight(Flight flight) {
    flightsService.createNewFlight(flight);

  }

  public List<Flight> searchFlight(String destination, String date, int seats) {
    return flightsService.searchFlight(destination, date, seats);
  }


  public Optional<Flight> getFlightById(long id) {
    return flightsService.getFlightById(id);
  }

  public long makeId(String time, String date, PlaceDestination placeDestination) {
    return flightsService.makeId(time, date, placeDestination);
  }

  public void loadData() {
    flightsService.loadData();
  }
}
