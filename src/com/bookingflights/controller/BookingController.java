package com.bookingflights.controller;

import com.bookingflights.entities.booking.Booking;
import com.bookingflights.entities.flight.Flight;
import com.bookingflights.entities.user.User;
import com.bookingflights.exceptions.IdNotFound;
import com.bookingflights.service.BookingService;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class BookingController {
  private final BookingService bookingService;


  public BookingController(BookingService bookingService) {
    this.bookingService = bookingService;
  }

  public List<Booking> getAllBookings() {
    return bookingService.getAllBookings();
  }

  public void displayAllBookings() {
    bookingService.displayAllBookings();
  }

  public Booking createNewBooking(int id, List<User> users, Flight flight, int seats) {
    return bookingService.createNewBooking(id, users, flight, seats);
  }

  public int generateId() {
    return bookingService.generateId();
  }

  public void cancelBooking(int index) throws IdNotFound {
    bookingService.cancelBooking(index);

  }

  public Optional<Booking> getBookingId(int id) {
    return bookingService.getBookingId(id);
  }

  public Optional<Booking> getUser(String name, String surname) {
    return bookingService.getUser(name, surname);
  }

  public void findBookings(String name, String surname) {
    if (this.getUser(name, surname).isEmpty()) {
      System.out.println("No user found");
    } else {
      bookingService.findBookings(name, surname);
    }
  }

  public List<Booking> loadData() throws IOException {
    return bookingService.loadData();
  }
}