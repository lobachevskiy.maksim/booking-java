package com.bookingflights.service;

import com.bookingflights.dao.FlightDao;
import com.bookingflights.entities.flight.Flight;
import com.bookingflights.entities.flight.PlaceDestination;

import java.util.List;
import java.util.Optional;

public class FlightsService {
  private FlightDao flightDao;


  public FlightsService(FlightDao flightDao) {
    this.flightDao = flightDao;
  }

  public List<Flight> getAllFlights() {
    return flightDao.getAllFlights();
  }

  public void displayAllFlights() {
    flightDao.getAllFlights().stream().forEach(flight -> System.out.println(flight));
  }

  public void createNewFlight(Flight flight) {
    flightDao.saveFlight(flight);

  }

  public long makeId(String time, String date, PlaceDestination placeDestination) {
    return flightDao.makeId(time, date, placeDestination);
  }

  public List<Flight> searchFlight(String destination, String date, int seats) {
    return flightDao.searchFlight(destination, date, seats);
  }


  public Optional<Flight> getFlightById(long id) {
    return flightDao.getFlightById(id);
  }

  public void loadData() {
    flightDao.loadData();
  }


}
