package com.bookingflights.service;

import com.bookingflights.dao.BookingDao;
import com.bookingflights.entities.booking.Booking;
import com.bookingflights.entities.flight.Flight;
import com.bookingflights.entities.user.User;
import com.bookingflights.exceptions.IdNotFound;

import java.io.IOException;
import java.util.*;

public class BookingService {
  public BookingDao bookingDao;

  public BookingService(BookingDao bookingDao) {
    this.bookingDao = bookingDao;
  }

  public List<Booking> getAllBookings() {
    return bookingDao.getAllBookings();
  }

  public void displayAllBookings() {
    for (int i = 0; i < this.getAllBookings().size(); i++) {
      System.out.println(getAllBookings().get(i).toString());
    }
  }

  public int generateId() {
    return bookingDao.generateId();
  }

  public Booking createNewBooking(int id, List<User> users, Flight flight, int seats) {
    return bookingDao.saveBooking(new Booking(id, users, flight, seats));
  }

  public void cancelBooking(int index) throws IdNotFound {
    bookingDao.cancelBooking(index);
  }

  public Optional<Booking> getBookingId(int id) {
    return bookingDao.getBookingId(id);
  }

  public Optional<Booking> getUser(String name, String surname) {
    User user = new User(name, surname);
    return this.getAllBookings().stream().filter(booking -> (booking.getUsers().contains(user))).findFirst();
  }


  public void findBookings(String name, String surname) {
    User user = new User(name.toLowerCase(), surname.toLowerCase());
    this.getAllBookings().stream().filter(booking -> (booking.getUsers().contains(user))).forEach(System.out::println);
  }

  public List<Booking> loadData() throws IOException {
    return bookingDao.loadData();
  }
}
