package com.bookingflights.entities.supportClasses;

import com.bookingflights.controller.BookingController;
import com.bookingflights.controller.FlightsController;
import com.bookingflights.entities.flight.Flight;
import com.bookingflights.entities.flight.PlaceDestination;
import com.bookingflights.entities.user.User;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class BookingFilteredFlights {
  private FlightsController flightsController;
  private BookingController bookingController;

  public BookingFilteredFlights() {
  }

  public BookingFilteredFlights(FlightsController flightsController,
                                BookingController bookingController) {
    this.flightsController = flightsController;
    this.bookingController = bookingController;
  }

  public BookingController getBookingController() {
    return bookingController;
  }

  public void setBookingController(BookingController bookingController) {
    this.bookingController = bookingController;
  }

  public FlightsController getFlightsController() {
    return flightsController;
  }

  public void setFlightsController(FlightsController flightsController) {
    this.flightsController = flightsController;
  }

  public String checkDateSearch() {
    String validStrInput = cheekStringInput();
    try {
      LocalDate.parse(validStrInput);
      return validStrInput;
    } catch (NullPointerException | DateTimeParseException e) {
      System.out.println(validStrInput + "- is in the wrong format. Please enter the date in the correct format");

      return checkDateSearch();
    }

  }

  public String checkEnumSearch() {
    String validStrInput = cheekStringInput();

    EnumSet<PlaceDestination> collectEnum = EnumSet.allOf(PlaceDestination.class);

    if (collectEnum.stream().anyMatch(e -> e.toString().equals(validStrInput))) {
      return validStrInput;
    } else {
      System.out.println("Incorrect name destination, try again");

      return checkEnumSearch();
    }

  }

  public int checkSeatsSearch(String seats, Scanner in) {
    try {
      return Integer.parseInt(seats);
    } catch (NumberFormatException e) {
      System.out.println(seats + "- it is not a number");
      String destination = in.nextLine();
      checkSeatsSearch(destination, in);
    }
    return 1;
  }

  public String cheekStringInput() {

    Scanner in = new Scanner(System.in);
    if (!in.hasNextInt()) {
      return in.nextLine();
    } else {
      System.out.println("You entered a wrong , try again!");
      return cheekStringInput();
    }
  }

  public static int cheekNumberInput() {

    Scanner in = new Scanner(System.in);
    if (in.hasNextInt()) {
      return in.nextInt();
    } else {
      System.out.println("You entered a wrong , try again!");
      return cheekNumberInput();
    }
  }

  public static int checkChooseFlight(Map<Integer, Flight> filterArr) {
    int validIntInput = cheekNumberInput();


    if (validIntInput >= 0 && validIntInput <= filterArr.size()) {
      return validIntInput;
    } else {
      System.out.println("Incorrect int flight, try again");
      return checkChooseFlight(filterArr);
    }

  }

  public User createUserConsole(int numHum) {
    Scanner scanner = new Scanner(System.in);

    System.out.println("Keep a record " + (numHum) + "- passenger");
    System.out.println("Name:");
    String name = scanner.nextLine();
    System.out.println("Last name:");

    String lastName = scanner.nextLine();
    return new User(name, lastName);

  }


  public void searchAndBookFlight() {
    Scanner input = new Scanner(System.in);
    System.out.println("Please, enter flight destination ");
    String validDestination = checkEnumSearch();
    System.out.println("Please, enter a date in format yyyy-MM-dd ");
    String validDate = checkDateSearch();
    System.out.println("Please, enter number of seats");

    String seats = input.nextLine();

    int validSeats = checkSeatsSearch(seats, input);
    List<Flight> filterArr = flightsController.searchFlight(validDestination, validDate, validSeats);
    if (filterArr.size() >= 1 && filterArr.size() != flightsController.getAllFlights().size()) {
      AtomicInteger count = new AtomicInteger();
      Map<Integer, Flight> flightMap = filterArr.stream().collect(Collectors.toMap(i -> count.getAndIncrement() + 1, p -> p));

      System.out.println("Please, enter number of flight or return main menu");
      System.out.println("0 Return main menu");
      flightMap.forEach((k, v) -> System.out.println(k + " " + v));

      int flight = checkChooseFlight(flightMap);

      if (flight == 0) {
        System.out.println(
                "You can monitor flights using the flight id on tab 2 of the main menu . "
                        + "\n" + "Choosing the flight that suits you best. !"
                        + "\n" + "The flight ID to " + validDestination + " for that day will appear under this post if there are flights on that route."

        );
        flightsController.getAllFlights().stream().filter(f ->
                f.getDateTime().toLocalDate().toString().equals(validDate) &&
                        f.getPlaceDestination().equals(validDestination)).forEach(f -> System.out.println("ID Flight:" + f.getId()));

      } else {
        List<User> users = new ArrayList<>();
        Flight currentFlight = flightMap.get(flight);
        for (int i = 1; i <= validSeats; i++) {

          users.add(createUserConsole(i));
          int bookingId = bookingController.generateId();
          if (bookingController.getBookingId(bookingId).isPresent()) {
            bookingId = bookingController.generateId();
          }
          bookingController.createNewBooking(bookingId, users, currentFlight, Integer.parseInt(seats));

        }
        currentFlight.setFreePlaces(currentFlight.getFreePlaces() - Integer.parseInt(seats));
        System.out.println("You have successfully booked your flight");
      }

    } else {
      System.out.println("At present for the period from - " + validDate + " to - " + validDestination + " there are flights " +
              "with the following number of places:");

      flightsController.getAllFlights().stream().filter(f -> f.getDateTime().toLocalDate().toString().equals(validDate) &&
              f.getPlaceDestination().equals(validDestination)).forEach(f -> System.out.println(f.getFreePlaces()));

      ;
    }


  }
}
