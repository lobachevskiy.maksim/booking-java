package com.bookingflights.entities.booking;

import com.bookingflights.entities.flight.Flight;
import com.bookingflights.entities.user.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Booking implements Serializable {
  private List<User> users = new ArrayList<>();
  private Flight flight;
  private int seats;
  private int id;

  public Booking() {
  }

  public Booking(int id, List<User> users, Flight flight, int seats) {
    this.users = users;
    this.flight = flight;
    this.seats = seats;
    this.id = id;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public List<User> getUsers() {
    return users;
  }

  public void setUsers(List<User> users) {
    this.users = users;
  }

  public Flight getFlight() {
    return flight;
  }

  public void setFlight(Flight flight) {
    this.flight = flight;
  }

  public int getSeats() {
    return seats;
  }

  public void setSeats(int seats) {
    this.seats = seats;
  }

  public void addUser(User user) {
    this.users.add(user);
  }

  @Override
  public String toString() {
    return String.format("\nBooking ID: %d, \nFlight to: %s\t Date: %-10s \tFlight ID: %d \nPassengers: %s \nSeats: %s",
            this.getId(),
            this.flight.getPlaceDestination(), this.flight.getDateTime(), this.flight.getId(),
            this.getUsers().toString().replace("[", "").replace("]", "").replace(",", ""), this.getSeats());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Booking booking)) return false;
    return Objects.equals(getUsers(), booking.getUsers());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getUsers());
  }
}
