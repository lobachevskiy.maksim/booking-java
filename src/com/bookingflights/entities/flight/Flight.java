package com.bookingflights.entities.flight;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Flight implements Serializable {
  private long id;
  private LocalDateTime dateTime;
  private int freePlaces;
  private String placeDestination;

  public Flight() {

  }


  public Flight(long id, String date, String time, int freePlaces, PlaceDestination placeDestination) {
    this.id = id;
    this.dateTime = parseDate(date, time);

    this.freePlaces = freePlaces;
    this.placeDestination = placeDestination.toString();
  }

  public long getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public LocalDateTime getDateTime() {
    return dateTime;
  }

  public void setDateTime(LocalDateTime dateTime) {
    this.dateTime = dateTime;
  }

  public int getFreePlaces() {
    return freePlaces;
  }

  public void setFreePlaces(int freePlaces) {
    this.freePlaces = freePlaces;
  }

  public String getPlaceDestination() {
    return placeDestination;
  }

  public void setPlaceDestination(String placeDestination) {
    this.placeDestination = placeDestination;
  }

  public String formatDate(LocalDateTime date) {
    DateTimeFormatter localDateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    String parsingDate = date.format(localDateFormat);
    return parsingDate;

  }

  public LocalDateTime parseDate(String date, String time) {
    try {
      LocalDate localDate = LocalDate.parse(date);
      LocalTime localTime = LocalTime.parse(time);

      return LocalDateTime.of(localDate, localTime);
    } catch (NullPointerException e) {
      System.out.println("Unsmoothed with " + date + " or " + time);
    }
    return null;
  }


  @Override
  public String toString() {
    return String.format("DESTINATION: %-10s DEPARTURE from: Kiev \tDEPARTURE DATE: %-10s \tFREE PLACES: %d \tFLIGHT ID: %-10o",
            placeDestination, formatDate(dateTime), freePlaces, id);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Flight flight = (Flight) o;
    return id == flight.id && freePlaces == flight.freePlaces && Objects.equals(dateTime, flight.dateTime) && Objects.equals(placeDestination, flight.placeDestination);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, dateTime, freePlaces, placeDestination);
  }


}

