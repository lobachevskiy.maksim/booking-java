package com.bookingflights.entities.user;

import java.io.Serializable;

public class User implements Serializable {
  private String name;
  private String surname;


  public User() {
  }

  public User(String name, String surname) {
    this.name = name;
    this.surname = surname;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  //  @Override
//  public boolean equals(Object o) {
//    if (this == o) return true;
//    if (!(o instanceof User user)) return false;
//    return Objects.equals(getName(), user.getName()) && Objects.equals(getSurname(), user.getSurname());
//  }
//
//  @Override
//  public int hashCode() {
//    return Objects.hash(getName(), getSurname());
//  }
  @Override
  public int hashCode() {
    int name = this.getName().hashCode();
    int surname = this.getSurname().hashCode();
    return name + surname;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!(obj instanceof User user))
      return false;
    return (name.hashCode() == user.getName().toLowerCase().hashCode() && surname.hashCode() == user.getSurname().toLowerCase().hashCode());
  }

  @Override
  public String toString() {
    return String.format("\nName: %s, Surname: %s", this.getName(), this.getSurname());
  }

}
