package com.bookingflights.console;

import com.bookingflights.controller.BookingController;
import com.bookingflights.controller.FlightsController;
import com.bookingflights.dao.CollectionBookingDao;
import com.bookingflights.dao.CollectionFlightDao;
import com.bookingflights.dao.FlightDao;
import com.bookingflights.entities.flight.Flight;
import com.bookingflights.entities.flight.PlaceDestination;
import com.bookingflights.entities.supportClasses.BookingFilteredFlights;
import com.bookingflights.exceptions.IdNotFound;
import com.bookingflights.exceptions.WrongOptionException;
import com.bookingflights.service.BookingService;
import com.bookingflights.service.FlightsService;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.bookingflights.entities.supportClasses.BookingFilteredFlights.checkChooseFlight;

public class ConsoleMenu {

  public static Scanner input = new Scanner(System.in);
  public static CollectionBookingDao bookingDao = new CollectionBookingDao();
  public static BookingService bookingService = new BookingService(bookingDao);
  public static BookingController bookingController = new BookingController(bookingService);

  public static FlightDao collectionFlightDao = new CollectionFlightDao();
  public static FlightsService flightsService = new FlightsService(collectionFlightDao);
  public static FlightsController flightsController = new FlightsController(flightsService);
  public static BookingFilteredFlights bookingFilteredFlights = new BookingFilteredFlights(flightsController, bookingController);

  public static Map<Integer, String> menuOptions() {
    Map<Integer, String> menu = new HashMap<>();
    menu.put(1, "Online Flight Board");
    menu.put(2, "Flight Info");
    menu.put(3, "Search and Book a Flight");
    menu.put(4, "Cancel Booking");
    menu.put(5, "My Flights");
    menu.put(6, "Exit");
    return menu;
  }


  public static String convertMenuToString() {
    System.out.println("MAIN MENU");
    return menuOptions().keySet().stream()
            .map(key -> key + ". " + menuOptions().get(key) + "\n")
            .collect(Collectors.joining(""));
  }

  public static void selectMenuItem() throws IOException {
    boolean exit = false;
    createFlight();
    bookingController.loadData();

    do {
      System.out.print(convertMenuToString());
      System.out.println("Please, make your choice: ");
      String optionUser = input.next().toLowerCase();
      input.nextLine();
      try {
        int option = Integer.parseInt(optionUser);
        if (option > 6) {
          throw new WrongOptionException("Incorrect option");
        }
      } catch (WrongOptionException | NumberFormatException e) {
        System.out.println("Incorrect option");
      }
      switch (optionUser) {
        case "1" -> showAllFlights();
        case "2" -> searchFlightIdOrIndex();
        case "3" -> bookingFilteredFlights.searchAndBookFlight();
        case "4" -> cancelBooking();
        case "5" -> myFlights();
        case "6" -> exit = true;
      }

    } while (!exit);

  }

  public static void createFlight() {
    List<PlaceDestination> destList = List.of(PlaceDestination.values());

    for (int i = 1; i <= 800; i++) {
      int indexDestination = (int) (Math.random() * destList.size());
      int frPlaces = (int) (Math.random() * (350 - 200 + 1) + 200);
      LocalTime time1 = LocalTime.of(0, 0);
      LocalTime time2 = LocalTime.of(23, 0);
      int secondOfDayTime1 = time1.toSecondOfDay();
      int secondOfDayTime2 = time2.toSecondOfDay();
      Random random = new Random();
      int randomSecondOfDay = secondOfDayTime1 + random.nextInt(secondOfDayTime2 - secondOfDayTime1);
      LocalTime randomLocalTime = LocalTime.ofSecondOfDay(randomSecondOfDay);
      LocalDate from = LocalDate.of(2021, 11, 16);
      LocalDate to = LocalDate.of(2021, 11, 25);
      long days = from.until(to, ChronoUnit.DAYS);
      long randomDays = ThreadLocalRandom.current().nextLong(days + i);
      LocalDate randomDate = from.plusDays(randomDays);
      String date = randomDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")).toString();

      Flight flight = new Flight(flightsController.makeId(randomLocalTime.toString(), date, destList.get(indexDestination)), date,
              randomLocalTime.toString(),
              frPlaces,
              destList.get(indexDestination));
      List<Flight> currentFlights = new ArrayList<>(List.of(flight));
      currentFlights.stream().forEach(f -> flightsController.createNewFlight(f));
    }
    flightsController.loadData();

  }

  public static Map<Integer, Flight> flightsToDay() {

    AtomicInteger count = new AtomicInteger();
    if (flightsController.getAllFlights().stream().anyMatch(f -> f.getDateTime().toLocalDate().equals(LocalDate.now()))) {
      return flightsController.getAllFlights().stream()
              .filter(f -> f.getDateTime().toLocalDate().equals(LocalDate.now())).collect(Collectors.toMap(i -> count.getAndIncrement() + 1, p -> p));


    } else {
      return Map.of();
    }
  }


  public static void showAllFlights() {
    System.out.println("Flight information board for today:");
    if (flightsToDay().size() >= 1) {
      flightsToDay().forEach((k, v) -> System.out.println(k + " " + v));
    } else {
      System.out.println("There are no flights for that day");
    }
  }

  public static void searchFlightIdOrIndex() {
    Scanner in = new Scanner(System.in);
    System.out.println("If you want to do an ID search press - 1 ");
    System.out.println("If by sequence number press - 2(only to search for flights for today if there are any)");
    switch (in.nextLine()) {
      case "1" -> showFlightInfoID();
      case "2" -> showFlightInfoIndex();
      default -> System.out.println("You have selected the wrong item .");
    }

  }

  public static void showFlightInfoIndex() {
    System.out.println("Select a flight by sequence number from today's flight list:");
    if (flightsToDay().size() >= 1) {
      int flight = checkChooseFlight(flightsToDay());
      System.out.println(flightsToDay().get(flight).toString());
      ;
    } else {
      System.out.println("There are no flights for that day");
    }

  }

  public static void showFlightInfoID() {
    Scanner in = new Scanner(System.in);
    System.out.println("Please, enter your Flight ID:");
    long ID;
    try {
      if (in.hasNextLong()) {
        ID = in.nextLong();
        if (flightsController.getFlightById(ID).isEmpty()) {
          throw new IdNotFound("Id not found, try again or exit to main menu");
        }
        System.out.println(flightsController.getFlightById(ID).get());

      } else {
        throw new InputMismatchException("Id incorrect, try again or exit to main menu");
      }

    } catch (IdNotFound | InputMismatchException e) {
      System.out.println(e.getMessage());
      System.out.println("Write (exit) to go to the main menu or any (symbol) to continue");
      in.nextLine();
      String choice = in.nextLine();
      if (!choice.equals("exit")) {
        showFlightInfo();
      }

    }

  }

  public static void showFlightInfo() {
    Scanner in = new Scanner(System.in);
    System.out.println("Please, enter your Flight ID:");
    long ID;
    try {
      if (in.hasNextLong()) {
        ID = in.nextLong();
        if (flightsController.getFlightById(ID).isEmpty()) {
          throw new IdNotFound("Id not found, try again or exit to main menu");
        }
        System.out.println(flightsController.getFlightById(ID).get());

      } else {
        throw new InputMismatchException("Id incorrect, try again or exit to main menu");
      }

    } catch (IdNotFound | InputMismatchException e) {
      System.out.println(e.getMessage());
      System.out.println("Write (exit) to go to the main menu or any (symbol) to continue");
      in.nextLine();
      String choice = in.nextLine();
      if (!choice.equals("exit")) {
        showFlightInfo();
      }
    }
  }


  public static void cancelBooking() {
    System.out.println("Please, enter your Booking ID:");
    String ID = input.next();
    try {
      int option = Integer.parseInt(ID);
      if (bookingController.getBookingId(option).isEmpty()) {
        throw new IdNotFound("Id not found");
      }
      int addSeats = bookingController.getBookingId(option).get().getSeats();
      Flight currentFlight = bookingController.getBookingId(option).get().getFlight();
      bookingController.cancelBooking(option);
      currentFlight.setFreePlaces(currentFlight.getFreePlaces() + addSeats);
      System.out.println("You have successfully cancelled your Booking");
    } catch (NumberFormatException | IdNotFound nfe) {
      System.out.println("Incorrect ID");
      cancelBooking();
    }

  }

  private static void myFlights() {
    System.out.println("Please, enter your name:");
    String name = input.next().toLowerCase();
    System.out.println("Please, enter your surname:");
    String surname = input.next().toLowerCase();

    bookingController.findBookings(name, surname);
  }

  public static void runApp() throws IOException {
    selectMenuItem();
  }

}

