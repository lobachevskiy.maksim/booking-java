package com.bookingflights.exceptions;

public class WrongOptionException extends Exception {
  public WrongOptionException(String msg) {
    super(msg);
  }
}
