package com.bookingflights.exceptions;

public class IdNotFound extends Exception {
  public IdNotFound(String msg) {
    super(msg);
  }
}
