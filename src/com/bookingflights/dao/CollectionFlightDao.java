package com.bookingflights.dao;

import com.bookingflights.entities.flight.Flight;
import com.bookingflights.entities.flight.PlaceDestination;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CollectionFlightDao implements FlightDao {
  private List<Flight> flightsList = new ArrayList<>(List.of());


  public void setFlightsList(List<Flight> flightsList) {
    this.flightsList = flightsList;


  }

  @Override
  public List<Flight> getAllFlights() {
    return flightsList;
  }

  @Override
  public Optional<Flight> getFlightById(long id) {
    return flightsList.stream().filter(f -> f.getId() == id).findFirst();


  }

  @Override
  public void deleteFlight(int id) {
    flightsList.removeIf(b -> b.getId() == id);
  }

  @Override
  public List<Flight> searchFlight(String destination, String date, int seats) {

    if (flightsList.stream().noneMatch(f ->
            f.getDateTime().toLocalDate().toString().equals(date) &&
                    f.getPlaceDestination().equals(destination) && f.getFreePlaces() >= seats
    )) {
      if (flightsList.stream().noneMatch(f ->
              f.getFreePlaces() >= seats)) {
        return flightsList;

      }

    }
    return flightsList.stream().filter(f ->
            f.getDateTime().toLocalDate().toString().equals(date) &&
                    f.getPlaceDestination().equals(destination) && f.getFreePlaces() >= seats
    ).toList();
  }


  @Override
  public void loadData() {

    try (FileInputStream fileInputStream = new FileInputStream("src/com/bookingflights/flightsList.txt");
         ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
      Flight flight = (Flight) objectInputStream.readObject();
      if (flightsList.contains(flight)) {
        int indexList = flightsList.indexOf(flight);
        flightsList.set(indexList, flight);
      } else {
        flightsList.add(flight);
      }
    } catch (FileNotFoundException | ClassNotFoundException | EOFException e) {
      e.printStackTrace(System.out);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public long makeId(String time, String date, PlaceDestination placeDestination) {
    String[] timeArr = time.split(":");
    String[] dateArr = date.split("-");
    Integer[] intArr = new Integer[timeArr.length + dateArr.length + 1];
    int count = 0;
    for (int i = 0; i < timeArr.length; i++) {
      intArr[i] = Integer.parseInt(timeArr[i]);
      count++;
    }
    for (int j = 0; j < dateArr.length; j++) {
      intArr[count++] = Integer.parseInt(dateArr[j]);
    }

    intArr[intArr.length - 1] = placeDestination.ordinal();
    StringBuilder strId = new StringBuilder();
    for (int num : intArr) {
      strId.append(num);
    }
    return Long.parseLong(strId.toString());
  }

  @Override
  public void saveFlight(Flight flight) {

    if (flightsList.contains(flight)) {
      int indexList = flightsList.indexOf(flight);
      flightsList.set(indexList, flight);
    } else {
      flightsList.add(flight);
    }
    ;
    try {
      FileOutputStream outputStream = new FileOutputStream("src/com/bookingflights/flightsList.txt");
      try {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(flight);
        objectOutputStream.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }
}
