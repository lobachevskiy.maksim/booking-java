package com.bookingflights.dao;

import com.bookingflights.entities.booking.Booking;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CollectionBookingDao implements BookingDao {

  private final List<Booking> bookingList = new ArrayList<>();

  public CollectionBookingDao() {
  }

  @Override
  public List<Booking> getAllBookings() {
    return this.bookingList;
  }

  @Override
  public Optional<Booking> getBookingId(int id) {
    return this.getAllBookings().stream().
            filter(b -> b.getId() == id).
            findFirst();
  }

  public void writeBooking(List<Booking> bookings) {
    try (FileOutputStream fos = new FileOutputStream("src/com/bookingflights/DB.txt");
         ObjectOutputStream oos = new ObjectOutputStream(fos)) {
      oos.writeObject(bookings);
    } catch (IOException e) {
      e.printStackTrace(System.out);
    }
  }

  @Override
  public int generateId() {

    return (int) (Math.random() * 10000);
  }

  @Override
  public void cancelBooking(int id) {
    this.getAllBookings().removeIf(b -> b.getId() == id);
    this.writeBooking(this.getAllBookings());
  }

  @Override
  public Booking saveBooking(Booking booking) {
    int index = this.bookingList.indexOf(booking);
    if (this.bookingList.contains(booking)) {
      this.bookingList.set(index, booking);
    } else {
      this.bookingList.add(booking);
    }
    this.writeBooking(this.bookingList);
    return booking;
  }

  @Override
  public List<Booking> loadData() throws IOException {
    List<Booking> bookings = new ArrayList<>();
    try (FileInputStream fis = new FileInputStream("src/com/bookingflights/DB.txt");
         ObjectInputStream ois = new ObjectInputStream(fis)) {
      bookings = (List<Booking>) ois.readObject();
      this.getAllBookings().addAll(bookings);
    } catch (FileNotFoundException | ClassNotFoundException | EOFException e) {
      e.printStackTrace(System.out);
    }
    return bookings;
  }
}

