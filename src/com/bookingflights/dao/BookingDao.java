package com.bookingflights.dao;

import com.bookingflights.entities.booking.Booking;
import com.bookingflights.exceptions.IdNotFound;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface BookingDao {
  List<Booking> getAllBookings();

  Optional<Booking> getBookingId(int id);

  void cancelBooking(int id) throws IdNotFound;

  int generateId();

  Booking saveBooking(Booking booking);

  List<Booking> loadData() throws IOException;
}
