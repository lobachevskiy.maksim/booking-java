package com.bookingflights.dao;

import com.bookingflights.entities.flight.Flight;
import com.bookingflights.entities.flight.PlaceDestination;
import com.bookingflights.exceptions.IdNotFound;

import java.util.List;
import java.util.Optional;

public interface FlightDao {
  List<Flight> getAllFlights();

  Optional<Flight> getFlightById(long id);

  void deleteFlight(int id) throws IdNotFound;

  List<Flight> searchFlight(String destination, String date, int seats);

  long makeId(String time, String date, PlaceDestination placeDestination);

  void loadData();

  void saveFlight(Flight flight);
}
