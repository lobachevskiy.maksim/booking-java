package com.bookingflights;

import com.bookingflights.console.ConsoleMenu;

import java.io.IOException;

public class Main {
  public static void main(String[] args) throws IOException {
    ConsoleMenu.runApp();
  }
}
