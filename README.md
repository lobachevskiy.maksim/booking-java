# Booking Java Step Project

## Project Participants

- [ ] Maksim.L - Maksim Lobachevskiy
- [ ] Володимир Жуківський - Volodymyr Zhukivskyi
- [ ] Vadim Bevziuk - Vadim Bevziuk

## Tasks

- [ ] Maksim.L - Booking-related classes, Console menu
- [ ] Володимир Жуківський - Flight-related classes, Console menu
- [ ] Vadim Bevziuk - Tests